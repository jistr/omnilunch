#!/bin/bash

TEMPORARY_INVENTORY_BANNER="# this is a temporary generated file, will be deleted"

if [ ! -v DIR -o -z "$DIR" ]; then
    echo "DIR env variable not set or zero length"
    exit 1
fi

function set_vars() {
    if [ ! -v OMNILUNCH_USER ]; then
        OMNILUNCH_USER=$(id -un)
    fi
}

function check_vars() {
    if [ ! -v OMNILUNCH_HOST ]; then
        echo "Setting OMNILUNCH_HOST is mandatory."
        exit 1
    fi
    if [ "$OMNILUNCH_SPAWN_PLAYBOOK" == "playbooks/omnilunch.yml" ]; then
        echo "Will rsync $(du -hsL --exclude="$DIR/export" --exclude="$DIR/log" --exclude="$DIR/tmp" --exclude="*/.git/*" $DIR | awk '{ print $1; }')."
        echo "Will run as $OMNILUNCH_USER@$OMNILUNCH_HOST".
    fi
}

function generate_inventory_file() {
    if [ -e "$1" ]; then
        echo "$1 exists, not overwriting."
    fi

    if [ "$OMNILUNCH_HOST" == "localhost" ]; then
        ANSIBLE_CONNECTION=local
    else
        ANSIBLE_CONNECTION=ssh
    fi

    mkdir "$DIR/tmp" &> /dev/null || true
    echo "$TEMPORARY_INVENTORY_BANNER" > "$1"
    echo "[omnilunch_host]" >> "$1"
    echo "$OMNILUNCH_HOST  ansible_connection=$ANSIBLE_CONNECTION  ansible_user=$OMNILUNCH_USER omnilunch_user=$OMNILUNCH_USER" >> "$1"
}

# removes inventory file if it contains $TEMPORARY_INVENTORY_BANNER
# first argument is the inventory file to check and optionally remove
function remove_inventory_file_if_temporary() {
    if [ -z "$1" ]; then
        echo "get_inventory_file_path first argument has zero length"
        exit 1
    fi

    if grep "$TEMPORARY_INVENTORY_BANNER" "$1" &> /dev/null; then
        rm "$1"
    fi
}
