#!/bin/bash

set -euo pipefail

DIR=$(cd $(dirname "$BASH_SOURCE[0]") && pwd)

export OMNILUNCH_SPAWN_PLAYBOOK=playbooks/omnilunch-destroy.yml
$DIR/omnilunch.sh "$@" -e "omnilunch_instance=$OMNILUNCH_INSTANCE"
