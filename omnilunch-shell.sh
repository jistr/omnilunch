#!/bin/bash

set -euo pipefail

DIR=$(cd $(dirname "$BASH_SOURCE[0]") && pwd)

exec bash "$DIR/tmp/shell-$OMNILUNCH_INSTANCE-$OMNILUNCH_HOST-${OMNILUNCH_USER:-$USER}"
