#!/bin/bash

set -euo pipefail

DIR=$(cd $(dirname "$BASH_SOURCE[0]") && pwd)
source "$DIR/lib/common.sh"
set_vars

OMNILUNCH_INVENTORY=${OMNILUNCH_INVENTORY:-"$DIR/tmp/inventory-$OMNILUNCH_HOST-$OMNILUNCH_USER"}
OMNILUNCH_SPAWN_PLAYBOOK=${OMNILUNCH_SPAWN_PLAYBOOK:-playbooks/omnilunch.yml}
check_vars

remove_inventory_file_if_temporary "$OMNILUNCH_INVENTORY"
generate_inventory_file "$OMNILUNCH_INVENTORY"

ansible-playbook \
    -i "$OMNILUNCH_INVENTORY" \
    -e "omnilunch_instance=$OMNILUNCH_INSTANCE" \
    -e "omnilunch_local_dir=$DIR" \
    "$@" \
    "$OMNILUNCH_SPAWN_PLAYBOOK"

remove_inventory_file_if_temporary "$OMNILUNCH_INVENTORY"

if [ "$OMNILUNCH_SPAWN_PLAYBOOK" = "playbooks/omnilunch.yml" ]; then
    exec $DIR/omnilunch-log-tailf.sh
fi
