#!/bin/bash

set -euo pipefail

DIR=$(cd $(dirname "$BASH_SOURCE[0]") && pwd)

export OMNILUNCH_SPAWN_PLAYBOOK=playbooks/omnilunch-log-tailf.yml
$DIR/omnilunch.sh "$@" -e "omnilunch_instance=$OMNILUNCH_INSTANCE"
exec bash "$DIR/tmp/log-tailf-$OMNILUNCH_INSTANCE-$OMNILUNCH_HOST-${OMNILUNCH_USER:-$USER}"
